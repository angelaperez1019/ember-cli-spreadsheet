import Ember from 'ember';
import WorkbooksMixin from 'ember-spreadsheet/mixins/workbooks';
import { module, test } from 'qunit';

module('Unit | Mixin | workbooks');

// Replace this with your real tests.
test('it works', (assert) => {
  let WorkbooksObject = Ember.Object.extend(WorkbooksMixin);
  let subject = WorkbooksObject.create();
  assert.ok(subject);
});

test('it can add a workbook to the workbook map', (assert) => {
	let WorkbooksObject = Ember.Object.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let testWorkbookName = 'test1.xlsx';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(testWorkbookName, testWorkbook);

	assert.ok(subject.get('workbooks').has(testWorkbookName));
});

test('it can remove a workbook from the map', (assert) => {
	let WorkbooksObject = Ember.Object.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let testWorkbookName = 'test1.xlsx';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(testWorkbookName, testWorkbook);
	subject.removeWorkbook(testWorkbookName);

	assert.notOk(subject.get('workbooks').has(testWorkbookName));
});

