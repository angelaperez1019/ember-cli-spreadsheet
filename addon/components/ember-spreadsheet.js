import Ember from 'ember';
import layout from '../templates/components/ember-spreadsheet';
import computed from 'ember-new-computed';
import workbooksMixin from '../mixins/ember-spreadsheet-workbooks';
import { read } from 'xlsx';

const MIME_TYPES = {
	XLS: 'application/vnd.ms-excel',
	XLSX: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
	XLSB: 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
	ODS: 'application/vnd.oasis.opendocument.spreadsheet',
	CSV: 'text/csv'
};

const WorkbooksObject = Ember.Object.extend(workbooksMixin);

export default Ember.Component.extend({
	layout,
	classNames: ['ember-spreadsheet'],

	////////////////////////////////////
	// Options Passed in to Component //
	////////////////////////////////////

	/**
	 * This determines what type of files can be imported.
	 *
	 * @property allowedFileTypes
	 * @default [MIME_TYPES.XLS, MIME_TYPES.XLSX, MIME_TYPES.XLSB, MIME_TYPES.ODS, MIME_TYPES.CSV]
	 * @type Ember.NativeArray
	 */
	allowedFileTypes: Ember.A([MIME_TYPES.XLS, MIME_TYPES.XLSX, MIME_TYPES.XLSB, MIME_TYPES.ODS, MIME_TYPES.CSV]),

	/**
	 * This dertimines if a user is able to upload more than one file at a time.
	 * 
	 * @property allowMultipleFiles
	 * @default false
	 * @type Boolean
	 */
	allowMultipleFiles: false,

	/**
	 * If this property is set to true, then the file will be imported and parsed
	 * without the user having to click an "import" button.
	 *
	 * @property automaticImport
	 * @default false
	 * @type Boolean
	 */
	automaticImport: false,

	///////////////////////////////////
	// Internal Properties and State //
	///////////////////////////////////

	/**
	 * This is the array of raw File objects (https://developer.mozilla.org/en-US/docs/Web/API/File).
	 * 
	 * @property files
	 * @default []
	 * @type Ember.NativeArray
	 */
	files: Ember.A(),

	/**
	 * The workbooks object is made up of each parsed workbook that was imported.
	 * 
	 * @type {Ember.Object}
	 */
	workbooks: WorkbooksObject.create(),

	/**
	 * The import button will be shown when this true.
	 *
	 * @property _showImportButton
	 * @private
	 * @readOnly
	 * @default false
	 * @type Boolean
	 */
	_showImportButton: computed('files', {
		get() {
			const files = this.get('files');
			let automaticImport = this.get('automaticImport');

			return (files.length && !automaticImport) ? true : false; 
		}
	}).readOnly(),

	/**
	 * Used to tell the input element what file types ot limit the search for.
	 * 
	 * @property _accept
	 * @private
	 * @readOnly
	 * @default ''
	 * @type String
	 */
	_accept: computed('allowedFileTypes', {
		get() {
			let allowedFileTypes = this.get('allowedFileTypes');
			return allowedFileTypes.join(',');
		}
	}).readOnly(),

	/////////////////////
	// Private Methods //
	/////////////////////

	/**
	 * Checks to see if file(s) chosen by user follow parameters given.
	 *
	 * @method _validateFiles
	 * @private
	 * @return {Object} object has a failed and reason property
	 */
	_validateFiles() {
		let xlsx = this.get('xlsx'),
				files = this.get('files'),
				allowedFileTypes = this.get('allowedFileTypes'),
				allowMultipleFiles = this.get('allowMultipleFiles');

		if (!files) {
			return {
				failed: true,
				reason: 'ember-spreadsheet: Validation of files before import failed because no files were found.'
			};
		}

		if (!allowMultipleFiles && files.length > 1) {
			return {
				failed: true,
				reason: 'ember-spreadsheet: Validation of files before import failed because more files were selected than allowed.'
			}
		}

		// check that file types match allowed file types
		for (let i = 0; i < files.length; ++i) {
			if (!allowedFileTypes.contains(files[i].type)) {
				return {
					failed: true,
					reason: 'ember-spreadsheet: Validation of files before import failed because one or more of the selected files was a disallowed file type.'
				}
			}
		}

		return {
			failed: false,
			reason: ''
		};
	},

	/**
	 * This function parses/reads selected files into workbook objects.
	 * 
	 * @method _importFiles
	 * @private
	 */
	_importFiles() {
		return new Ember.RSVP.Promise((resolve, reject) => {
			let workbooks = this.get('workbooks'),
					files = this.get('files'),
					validateFiles = this._validateFiles();
		
			if (validateFiles.failed) {
				throw new Error(validateFiles.reason);
			}

			for (let i = 0; i < files.length; ++i) {
				let reader = new FileReader(),
						file = files[i],
						fileName = file.name;
					
				reader.onload = (e) => {
					let data = e.target.result,
							workbook = read(data, {type: 'binary'});

					workbooks.addWorkbook(fileName, workbook);

					if (i == files.length - 1) {
						resolve();
					}
				};

				reader.readAsBinaryString(file);
			}
		});
	},

	actions: {
		/**
		 * This method exposes the private method _importFiles as an action.
		 * 
		 * @method importFiles
		 */
		importFiles() {
			this._importFiles().then(() => {
				this.sendAction('afterImport');
			});
		},

		/**
		 * This method retrieves the FileList object and sets the files property with it.
		 * 
		 * @param {Object} e - The event object.
		 */
		retrieveFile(e) {
			this.set('files', e.target.files);

			if (this.get('automaticImport')) {
				this.send('importFiles');
			}
		}	
	}
});
